import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {User} from '../user/user';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'jce-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  @Output() userAddedEvent = new EventEmitter<User>();
  user:User = {name: '', email:''};

  onSubmit(form:NgForm){
    console.log(form);
    this.userAddedEvent.emit(this.user);
    this.user = {
       name: '',
       email: ''
    }
  }
  constructor() { }

  ngOnInit() {
  }

}
