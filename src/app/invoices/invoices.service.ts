import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {AngularFire} from 'angularfire2';
import { Http } from '@angular/http';
import 'rxjs/add/operator/delay';


@Injectable()
export class InvoicesService {
  invoicesObservable;

   getInvoices(){
   this.invoicesObservable = this.af.database.list('/invoices');
   return this.invoicesObservable;
   
 }

   addInvoice(invoice){
   this.invoicesObservable.push(invoice);
  }

  constructor(private af:AngularFire) { }

}
