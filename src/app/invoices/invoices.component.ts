import { Component, OnInit } from '@angular/core';
 import { InvoicesService } from './invoices.service';


@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css']
})
export class InvoicesComponent implements OnInit {
  isLoading:Boolean = true;
  
  invoices;

  currentInvoice;

  select(invoice){
    this.currentInvoice = invoice;
  }

  constructor(private _invoicesService:InvoicesService) { }
    addInvoice(invoice){
    this._invoicesService.addInvoice(invoice);
  }

  ngOnInit() {
    this._invoicesService.getInvoices().subscribe(invoicesData => {this.invoices=invoicesData;
    this.isLoading=false});
  }

}
