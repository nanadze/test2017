import { Component, OnInit } from '@angular/core';
import { ProductsService } from './products.service';
import { AngularFire } from 'angularfire2';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products;

  deleteProduct(product){
    this._productsServise.deleteProduct(product);
  }

  constructor(private _productsServise: ProductsService) { }

  ngOnInit() {
    this._productsServise.getProducts()
    .subscribe(products => 
    {this.products = products});
    }
  }


